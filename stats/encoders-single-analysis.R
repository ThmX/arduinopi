require(ggplot2)
require(gridExtra)

finite.differences.forward <- function(x, y) {
  if (length(x) != length(y)) {
    stop('x and y vectors must have equal length')
  }
  
  n <- length(x)
  
  fdx <- vector(length = n)
  
  for (i in 2:n) {
    fdx[i-1] <- (y[i-1] - y[i]) # / (x[i-1] - x[i])
  }
  
  fdx[n] <- (y[n] - y[n - 1]) # / (x[n] - x[n - 1])
  
  return(fdx)
}

finite.differences.central <- function(x, y) {
  if (length(x) != length(y)) {
    stop('x and y vectors must have equal length')
  }
  
  n <- length(x)
  
  fdx <- vector(length = n)
  
  fdx[1] <- (y[1] - y[2]) # / (x[i-1] - x[i])
  
  for (i in 2:(n-1)) {
    fdx[i-1] <- (y[i-1] - y[i+1]) / 2 # / (x[i-1] - x[i])
  }
  
  fdx[n] <- (y[n] - y[n - 1]) # / (x[n] - x[n - 1])
  
  return(fdx)
}

filename <- 'dump4_sleep10'
data.raw <- read.csv(paste('data/', filename, '.csv', sep=''))
speed <- 100

compute.dt <- function(data.raw) {
  data <- data.raw[-c(1),]
  data$dt <- 1000 * diff(data.raw$timestamp)
  data
}

df.speed <- function(data.raw, speed) {
  data <- data.raw[data.raw$velocity_l == speed,]
  data[1:500,]
}



data.speed <- df.speed(data.raw, speed)

data.dt <- compute.dt(data.speed)


plot.diffs <- function(ts, xs) {
  ts <- data.speed$millis
  xs <- data.speed$ir_l
  
  dxs <- finite.differences.forward(ts, xs)
  ddxs <- finite.differences.forward(ts, dxs)
  
  df <- data.frame(ts, xs, dxs)
  px <- ggplot(df, aes(ts, xs)) + geom_line() + geom_hline(yintercept = 550)
  pdx <- ggplot(df, aes(ts, dxs)) + geom_line()
  pddx <- ggplot(df, aes(ts, ddxs)) + geom_line()
  
  grid.arrange(px, pdx, pddx, ncol = 1)
}

plot.diffs(data.speed$millis, data.speed$ir_l)
plot.diffs(data.speed$millis, data.speed$ir_r)

p1 <- ggplot(data.dt, aes(timestamp, steps_l)) + geom_line()
p2 <- ggplot(data.dt, aes(timestamp, ir_l)) + geom_line()

p3 <- ggplot(data.dt, aes(timestamp, steps_r)) + geom_line()
p4 <- ggplot(data.dt, aes(timestamp, ir_r)) + geom_line()

grid.arrange(p1, p2, p3, p4, ncol = 1)
ggsave(paste(filename, '-encoder-', speed, '.png', sep=''), arrangeGrob(p1, p2, p3, p4, ncol = 1), width = 8, height = 5)
