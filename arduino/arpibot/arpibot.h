#include <prismino.h>
#include <pid.h>

#ifndef _arpibot_h
#define _arpibot_h

// ------------------------------------------------------------------
// Commands
// -----------

const byte COMMAND_RESET = 0xA5;                 // W

// Motors
const byte COMMAND_MOTORS_STEPS = 0x10;          // R
const byte COMMAND_MOTORS_STEPS_LEFT = 0x11;     // R
const byte COMMAND_MOTORS_STEPS_RIGHT = 0x12;    // R

const byte COMMAND_MOTORS_VELOCITY = 0x14;       // W
const byte COMMAND_MOTORS_VELOCITY_LEFT = 0x15;  // W
const byte COMMAND_MOTORS_VELOCITY_RIGHT = 0x16; // W

// Servos
const byte COMMAND_SERVOS_LEFT = 0x21;           // W
const byte COMMAND_SERVOS_RIGHT = 0x22;          // W

// ------------------------------------------------------------------
// Pins
// -----------

const int sensorLeftPin = A1;
const int sensorRightPin = A2;

const int captorFrontPin = A3;
const int captorLeftPin = A4;
const int captorRightPin = A5;

// ------------------------------------------------------------------
// Robot constants
// -----------
const double ROBOT_RADIUS_MM = 115;
const double WHEEL_RADIUS_MM = 34;
const double WHEEL_RESOLUTION_RAD = PI / 24; // 24 black and white stripes
const double WHEEL_RESOLUTION_MM = WHEEL_RADIUS_MM * WHEEL_RESOLUTION_RAD;

const double WHEEL_RATIO = 0.9;

// ------------------------------------------------------------------
// Global variables
// -----------

static volatile uint16_t serial_id = 0;

static volatile uint16_t captor_f = 0;
static volatile uint16_t captor_r = 0;
static volatile int captor_l = 0;

static volatile int16_t ir_l = 0;
static volatile int16_t ir_r = 0;

static volatile int16_t steps_l = 0;
static volatile int16_t steps_r = 0;

static volatile uint16_t steps_stop_l = 0;
static volatile uint16_t steps_stop_r = 0;

static volatile int8_t velocity_target_l = 0;
static volatile int8_t velocity_current_l = 0;

static volatile int8_t velocity_target_r = 0;
static volatile int8_t velocity_current_r = 0;


void reset_arpibot();

// ------------------------------------------------------------------
// Serial RX/TX
// ------------

void serialTXInt8(int8_t payload);

void serialTXUInt8(uint8_t payload);

void serialTXInt16(int16_t payload);

void serialTXUInt16(uint16_t payload);

void serialTXUInt32(uint32_t payload);

void serialTX();

void serialRX();

// ------------------------------------------------------------------
// Buttons
// -----------

byte buttons();

// ------------------------------------------------------------------
// Captors
// -----------

void captors();

// ------------------------------------------------------------------
// Encoders
// -----------

void encoder_left();

void encoder_right();

void encoders();

// ------------------------------------------------------------------
// Motors
// -----------

void motors();

// ------------------------------------------------------------------

#endif
