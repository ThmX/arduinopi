#include "arpibot.h"

void reset_arpibot() {
    serial_id = 0;

    captor_f = 0;
    captor_r = 0;
    captor_l = 0;

    steps_l = 0;
    steps_r = 0;

    steps_stop_l = 0;
    steps_stop_r = 0;

    velocity_target_l = 0;
    velocity_current_l = 0;

    velocity_target_r = 0;
    velocity_current_r = 0;

    digitalWrite(LED, LOW);

    setSpeed(0, 0);
}

// ------------------------------------------------------------------
// Serial RX/TX
// ------------

void serialTXInt8(int8_t payload) {
    Serial.write(payload);
}

void serialTXUInt8(uint8_t payload) {
    Serial.write(payload);
}

void serialTXInt16(int16_t payload) {
    Serial.write(payload & 0x00FF);
    Serial.write((payload >> 8) & 0x00FF);
}

void serialTXUInt16(uint16_t payload) {
    Serial.write(payload & 0x00FF);
    Serial.write((payload >> 8) & 0x00FF);
}

void serialTXUInt32(uint32_t payload) {
    Serial.write(payload & 0x00FF);
    Serial.write((payload >> 8) & 0x00FF);
    Serial.write((payload >> 16) & 0x00FF);
    Serial.write((payload >> 24) & 0x00FF);
}

void serialTX() {
    serialTXUInt8(0xAA);
    serialTXUInt32(millis());
    serialTXUInt16(serial_id);
    serialTXInt16(buttons());
    serialTXInt16(steps_l);
    serialTXInt16(steps_r);
    serialTXInt16(steps_stop_l);
    serialTXInt16(steps_stop_r);
    serialTXInt16(ir_l);
    serialTXInt16(ir_r);
    serialTXInt8(velocity_current_l);
    serialTXInt8(velocity_current_r);
    serialTXUInt16(captor_l);
    serialTXUInt16(captor_r);
    serialTXUInt8(0x55);

    serial_id++;
}

void serialRX() {
    byte command = 0;
    int payload = 0;

    while (Serial.available() >= 3) {
        command = Serial.read();
        payload = 0x0000;
        payload |= 0x00FF & Serial.read();
        payload |= (0x00FF & Serial.read()) << 8;

        switch (command) {
            case COMMAND_MOTORS_STEPS:
                steps_stop_l = payload;
                steps_stop_r = payload;
                break;

            case COMMAND_MOTORS_STEPS_LEFT:
                steps_stop_l = payload;
                break;

            case COMMAND_MOTORS_STEPS_RIGHT:
                steps_stop_r = payload;
                break;

            case COMMAND_MOTORS_VELOCITY:
                velocity_target_l = payload;
                velocity_target_r = payload;
                break;

            case COMMAND_MOTORS_VELOCITY_LEFT:
                velocity_target_l = payload;
                break;

            case COMMAND_MOTORS_VELOCITY_RIGHT:
                velocity_target_r = payload;
                break;

            case COMMAND_SERVOS_LEFT:
                // TODO
                break;

            case COMMAND_SERVOS_RIGHT:
                // TODO
                break;

            default:
                // TODO Fix this
                while (Serial.available() > 0) {
                    Serial.read();
                }
                reset_arpibot();
                serialTXUInt8(0xA5);
                break;
        }

    }
}

// ------------------------------------------------------------------
// Buttons
// -----------

byte buttons() {
    byte btn = 0x00;

    btn |= digitalRead(BTN) ? 0x00 : 0x01;
    btn |= digitalRead(DIP1) ? 0x10 : 0x00;
    btn |= digitalRead(DIP2) ? 0x20 : 0x00;
    btn |= digitalRead(DIP3) ? 0x40 : 0x00;
    btn |= digitalRead(DIP4) ? 0x80 : 0x00;

    return btn;
}

// ------------------------------------------------------------------
// Captors
// -----------

void captors() {
    captor_l = analogRead(captorLeftPin);
    captor_r = analogRead(captorRightPin);
}

// ------------------------------------------------------------------
// Encoders
// -----------

void encoder_left() {
    static int ir0_l;
    static int ir1_l;

    static int dir0_l;
    static int dir1_l;

    static bool flipped_l;

    static double velocity_l;

    static unsigned long t0;
    static unsigned long t1;
    static long dt;

    ir0_l = analogRead(sensorLeftPin);
    dir0_l = ir1_l - ir0_l;

    if (abs(dir0_l) > 10) {
        flipped_l = true;
    }

    if (flipped_l && ((dir0_l > 0 && dir1_l < 0) || (dir0_l < 0 && dir1_l > 0))) {
        flipped_l = false;

        t0 = millis();

        dt = t0 - t1;
        velocity_l *= 0.8;
        if (velocity_target_l > 0) {
            steps_l++;
            velocity_l += 0.2 * 1000.0 / dt;
        } else {
            steps_l--;
            velocity_l -= 0.2 * 1000.0 / dt;
        }

        if (steps_stop_l > 0) {
            steps_stop_l--;
        }

        velocity_current_l = (int8_t) velocity_l;
        t1 = t0;
    }

    // Backup
    dir1_l = dir0_l;
    ir1_l = ir0_l;

    ir_l = ir0_l;
}

void encoder_right() {
    static int ir0_r;
    static int ir1_r;

    static int dir0_r;
    static int dir1_r;

    static bool flipped_r;

    static double velocity_r;

    static unsigned long t0;
    static unsigned long t1;
    static long dt;

    ir0_r = analogRead(sensorRightPin);
    dir0_r = ir1_r - ir0_r;

    if (abs(dir0_r) > 10) {
        flipped_r = true;
    }

    if (flipped_r && ((dir0_r > 0 && dir1_r < 0) || (dir0_r < 0 && dir1_r > 0))) {
        flipped_r = false;

        t0 = millis();

        dt = t0 - t1;
        velocity_r *= 0.8;
        if (velocity_target_r > 0) {
            steps_r++;
            velocity_r += 0.2 * 1000.0 / dt;
        } else {
            steps_r--;
            velocity_r -= 0.2 * 1000.0 / dt;
        }

        if (steps_stop_r > 0) {
            steps_stop_r--;
        }

        velocity_current_r = (int8_t) velocity_r;
        t1 = t0;
    }

    // Backup
    dir1_r = dir0_r;
    ir1_r = ir0_r;

    ir_r = ir0_r;
}

void encoders() {
    encoder_left();
    encoder_right();
}

// ------------------------------------------------------------------
// Motors
// -----------

void motors() {
    // TODO Use PID to go from current_velocity to target_velocity using the encoders

    const int8_t velocity_l = (steps_stop_l > 0) ? velocity_target_l : 0;
    const int8_t velocity_r = (steps_stop_r > 0) ? velocity_target_r : 0;
    setSpeed(velocity_l, (int8_t) (WHEEL_RATIO * velocity_r));
}

// ------------------------------------------------------------------
