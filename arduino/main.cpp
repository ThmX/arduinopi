#include <arpibot.h>

void setup() {
    Serial.begin(9600);
    pinMode(BTN, INPUT);
    pinMode(DIP1, INPUT);
    pinMode(DIP2, INPUT);
    pinMode(DIP3, INPUT);
    pinMode(DIP4, INPUT);

    reset_arpibot();
    setSpeed(0, 0);
}

// ------------------------------------------------------------------
// Main Loop
// -----------

void loop() {
    serialRX();

    captors();

    encoders();

    motors();

    serialTX();

    delay(10);
}
