# ArduinoPi - Arduino

## Remote Development with CLion
CLion provides a [full remote mode](https://www.jetbrains.com/help/clion/remote-projects-support.html) that allows you to develop locally while compiling, running and debugging your software remotely.

## Useful commands

### Reset Arduino
```bash
stty -F /dev/ttyACM0 1200
```
