#include <prismino.h>

// Robot constants
const float ROBOT_RADIUS_CM = 11.5; // cm
const float WHEEL_RADIUS_CM = 3.4; // cm
const float WHEEL_RESOLUTION_RAD = PI / 12;
const float WHEEL_RIGHT_NORMALIZER = 0.96;

// Pre-computed constants
const float WHEEL_PERIMETER_RESOLUTION_CM = WHEEL_RADIUS_CM * WHEEL_RESOLUTION_RAD;

//
const int sensorLeftPin = A1;
const int sensorRightPin = A2;

const int captorLeftPin = A4;
const int captorRightPin = A5;

int irSign_r = 1;
int irSign_l = 1;
int encCount_r = 0;
int encCount_l = 0;
int velocity_r = 0;
int velocity_l = 0;

void encoder() {
    static int ir_l;
    static int irOld_l;
    static int irEdge_l;

    static int ir_r;
    static int irOld_r;
    static int irEdge_r;

    // Left
    ir_l = analogRead(sensorLeftPin);
    if (irSign_l * (ir_l - irOld_l) < 0) {
        if (abs(ir_l - irEdge_l) > 100) {
            if (encCount_l-- <= 0) {
                velocity_l = 0;
                Serial.println("stop left");
            }
        }
        irEdge_l = irOld_l;
        irSign_l = -irSign_l;
    }

    // Right
    ir_r = analogRead(sensorRightPin);
    if (irSign_r * (ir_r - irOld_r) < 0) {
        if (abs(ir_r - irEdge_r) > 100) {
            if (encCount_r-- <= 0) {
                velocity_r = 0;
                Serial.println("stop right");
            }
        }
        irEdge_r = irOld_r;
        irSign_r = -irSign_r;
    }

    // Apply velocity
    setSpeed(velocity_l, WHEEL_RIGHT_NORMALIZER * velocity_r);

    // Backup
    irOld_l = ir_l;
    irOld_r = ir_r;
}

void captors() {
    int ir_l;
    int ir_r;

    // Left
    ir_l = analogRead(captorLeftPin);

    // Right
    ir_r = analogRead(captorRightPin);

    //Serial.print(ir_l);
    //Serial.print(",");
    //Serial.println(ir_r);
}

void rotate(float rad, int velocity) {
    if (rad < 0) {
        rad = -rad;
        velocity = -velocity;
    }

    encCount_r = int(rad * ROBOT_RADIUS_CM / WHEEL_PERIMETER_RESOLUTION_CM);
    velocity_r = velocity;

    encCount_l = int(rad * ROBOT_RADIUS_CM / WHEEL_PERIMETER_RESOLUTION_CM);
    velocity_l = -velocity;
}

void translate(float dist, int velocity) {
    if (dist < 0) {
        dist = -dist;
        velocity = -velocity;
    }

    encCount_r = int(dist / WHEEL_PERIMETER_RESOLUTION_CM);
    velocity_r = velocity;

    encCount_l = int(dist / WHEEL_PERIMETER_RESOLUTION_CM);
    velocity_l = velocity;
}

void pingpong() {
    int rdByte = 0;
    if (Serial.available() > 0) {
        rdByte = Serial.read();
        Serial.write(rdByte);
    }
}

int direction = 1;

void setup() {
    Serial.begin(9600);
    pinMode(BTN, INPUT);

    //pinMode(sensorLeftPin, INPUT);
    //setTimer(encoder, 1);
    translate(1, 10);
}

void loop() {
    if (!digitalRead(BTN)) {
        //translate(direction * -10, 50);
        rotate(direction * PI, 50);
        direction = -direction;
        delay(500);
    }

    captors();
    encoder();
    delay(10);
    pingpong();
}
