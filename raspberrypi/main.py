import logging
from time import time

import serial

logging.basicConfig(
    level=logging.INFO,
    format='%(asctime)s - %(levelname)s - %(name)s - %(message)s'
)


def run_experiment(ser, csv, speed):
    ser.write(int.to_bytes(0, 3, 'little'))
    logging.info(hex(int.from_bytes(ser.read(1), byteorder='little')))

    ser.write(b'\x14')
    ser.write(int.to_bytes(speed, 2, 'little', signed=True))

    steps_l = 0
    while steps_l < 1000:
        sync_start = int.from_bytes(ser.read(1), byteorder='little')
        millis = int.from_bytes(ser.read(4), byteorder='little')
        serial_idx = int.from_bytes(ser.read(2), byteorder='little')
        buttons = int.from_bytes(ser.read(2), byteorder='little')
        steps_l = int.from_bytes(ser.read(2), byteorder='little', signed=True)
        steps_r = int.from_bytes(ser.read(2), byteorder='little', signed=True)
        ir_l = int.from_bytes(ser.read(2), byteorder='little', signed=True)
        ir_r = int.from_bytes(ser.read(2), byteorder='little', signed=True)
        velocity_current_l = int.from_bytes(ser.read(1), byteorder='little', signed=True)
        velocity_current_r = int.from_bytes(ser.read(1), byteorder='little', signed=True)
        captor_l = int.from_bytes(ser.read(2), byteorder='little')
        captor_r = int.from_bytes(ser.read(2), byteorder='little')
        sync_stop = int.from_bytes(ser.read(1), byteorder='little')
        logging.info('{} - {}, idx {}, btn {}, steps {} {}, ir {} {}, {}, {}, {} - {}'.format(
            hex(sync_start),
            millis, serial_idx, hex(buttons),
            steps_l, steps_r, ir_l, ir_r,
            'velocity left {}'.format(velocity_current_l),
            'velocity right {}'.format(velocity_current_r),
            'captors {} {}'.format(captor_l, captor_r),
            hex(sync_stop)
        ))

        csv.write('{},{}\n'.format(time(), ','.join(map(str, [
            speed,
            millis, serial_idx,
            steps_l, steps_r, ir_l, ir_r,
            velocity_current_l,
            velocity_current_r,
            captor_l, captor_r
        ]))))


if __name__ == '__main__':
    try:
        with serial.Serial('/dev/ttyACM0', 9600) as ser:
            csv = open('data/dump11.csv', 'w')
            csv.write(
                'timestamp,speed,millis,serial_idx,steps_l,steps_r,ir_l,ir_r,velocity_current_l,velocity_current_r,captor_l,captor_r\n')

            logging.info(ser.name)

            speeds = [50]
            for speed in speeds:
                run_experiment(ser, csv, speed)

    except Exception as ex:
        logging.error(ex)
