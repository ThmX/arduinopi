from signal import pause

from sense_hat import SenseHat, ACTION_PRESSED, ACTION_HELD, ACTION_RELEASED

sense = SenseHat()


def imus():
    print('get_pressure {}'.format(sense.get_pressure()))
    print('get_humidity {}'.format(sense.get_humidity()))
    print('get_temperature_from_humidity {}'.format(sense.get_temperature_from_humidity()))
    print('get_temperature_from_pressure {}'.format(sense.get_temperature_from_pressure()))

    sense.set_imu_config(compass_enabled=True, gyro_enabled=True, accel_enabled=True)
    print('get_orientation_degrees {}'.format(sense.get_orientation_degrees()))

    print('get_accelerometer {}'.format(sense.get_accelerometer()))
    print('get_compass {}'.format(sense.get_compass()))
    print('get_gyroscope {}'.format(sense.get_gyroscope()))

    for event in sense.stick.get_events():
        print("The joystick was {} {}".format(event.action, event.direction))

    print('-------------------')


x = 3
y = 3


def clamp(value, min_value=0, max_value=7):
    return min(max_value, max(min_value, value))


def pushed_up(event):
    global y
    if event.action != ACTION_RELEASED:
        y = clamp(y - 1)


def pushed_down(event):
    global y
    if event.action != ACTION_RELEASED:
        y = clamp(y + 1)


def pushed_left(event):
    global x
    if event.action != ACTION_RELEASED:
        x = clamp(x - 1)


def pushed_right(event):
    global x
    if event.action != ACTION_RELEASED:
        x = clamp(x + 1)


def refresh():
    sense.clear()
    sense.set_pixel(x, y, 255, 255, 255)


sense.stick.direction_up = pushed_up
sense.stick.direction_down = pushed_down
sense.stick.direction_left = pushed_left
sense.stick.direction_right = pushed_right
sense.stick.direction_any = refresh
sense.stick.direction_middle = imus
refresh()
pause()
