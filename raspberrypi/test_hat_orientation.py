from time import sleep, time

from sense_hat import SenseHat

sense = SenseHat()
sense.set_imu_config(compass_enabled=True, gyro_enabled=True, accel_enabled=True)

with open('data/sensehat_rpi_dump3.csv', 'w') as csv:
    header = ','.join([
        'timestamp',
        'pressure',
        'humidity',
        'temperature_from_humidity',
        'temperature_from_pressure',
        'compass',
        'orientation_p', 'orientation_r', 'orientation_y',
        'accelerometer_p', 'accelerometer_r', 'accelerometer_y',
        'gyroscope_p', 'gyroscope_r', 'gyroscope_y'
    ])
    print(header)
    csv.write('{}\n'.format(header))

    while True:
        str = ','.join([
            '{}'.format(time()),
            '{}'.format(sense.get_pressure()),
            '{}'.format(sense.get_humidity()),
            '{}'.format(sense.get_temperature_from_humidity()),
            '{}'.format(sense.get_temperature_from_pressure()),
            '{}'.format(sense.get_compass()),
            '{},{},{}'.format(*sense.get_orientation_degrees().values()),
            '{},{},{}'.format(*sense.get_accelerometer().values()),
            '{},{},{}'.format(*sense.get_gyroscope().values()),
        ])
        print(str)
        csv.write('{}\n'.format(str))
        sleep(0.01)
