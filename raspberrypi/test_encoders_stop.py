import logging
from time import sleep, time

import serial

logging.basicConfig(
    level=logging.INFO,
    format='%(asctime)s - %(levelname)s - %(name)s - %(message)s'
)


class SerialRead(object):

    def __init__(self,
                 sync_start,
                 millis,
                 serial_idx,
                 buttons,
                 steps_l, steps_r,
                 steps_stop_l, steps_stop_r,
                 ir_l, ir_r,
                 velocity_current_l, velocity_current_r,
                 captor_l, captor_r,
                 sync_stop):
        self.sync_start = sync_start
        self.timestamp = time()
        self.millis = millis
        self.serial_idx = serial_idx
        self.buttons = buttons
        self.steps_l = steps_l
        self.steps_r = steps_r
        self.steps_stop_l = steps_stop_l
        self.steps_stop_r = steps_stop_r
        self.ir_l = ir_l
        self.ir_r = ir_r
        self.velocity_current_l = velocity_current_l
        self.velocity_current_r = velocity_current_r
        self.captor_l = captor_l
        self.captor_r = captor_r
        self.sync_stop = sync_stop

    def __str__(self):
        return '{} - {}, idx {}, {}, {}, {}, {}, {}, {} - {}'.format(
            hex(self.sync_start),
            self.millis, self.serial_idx,
            'buttons {}'.format(hex(self.buttons)),
            'steps {} {}'.format(self.steps_l, self.steps_r),
            'ir {} {}'.format(self.ir_l, self.ir_r),
            'stop {} {}'.format(self.steps_stop_l, self.steps_stop_r),
            'velocity left {}'.format(self.velocity_current_l),
            'velocity right {}'.format(self.velocity_current_r),
            'captors {} {}'.format(self.captor_l, self.captor_r),
            hex(self.sync_stop)
        )

    def csv(self):
        return ','.join(map(str, [
            self.timestamp,
            self.millis,
            self.serial_idx,
            self.buttons,
            self.steps_l, self.steps_r,
            self.steps_stop_l, self.steps_stop_r,
            self.ir_l, self.ir_r,
            self.velocity_current_l, self.velocity_current_r,
            self.captor_l, self.captor_r
        ]))

    @staticmethod
    def csv_header():
        return ','.join([
            'timestamp',
            'millis',
            'serial_idx',
            'buttons',
            'steps_l', 'steps_r',
            'steps_stop_l', 'steps_stop_r',
            'ir_l', 'ir_r',
            'velocity_current_l', 'velocity_current_r',
            'captor_l', 'captor_r'
        ])

    @staticmethod
    def from_tty(tty):
        return SerialRead(
            sync_start=int.from_bytes(tty.read(1), byteorder='little'),
            millis=int.from_bytes(tty.read(4), byteorder='little'),
            serial_idx=int.from_bytes(tty.read(2), byteorder='little'),
            buttons=int.from_bytes(tty.read(2), byteorder='little'),
            steps_l=int.from_bytes(tty.read(2), byteorder='little', signed=True),
            steps_r=int.from_bytes(tty.read(2), byteorder='little', signed=True),
            steps_stop_l=int.from_bytes(tty.read(2), byteorder='little'),
            steps_stop_r=int.from_bytes(tty.read(2), byteorder='little'),
            ir_l=int.from_bytes(tty.read(2), byteorder='little', signed=True),
            ir_r=int.from_bytes(tty.read(2), byteorder='little', signed=True),
            velocity_current_l=int.from_bytes(tty.read(1), byteorder='little', signed=True),
            velocity_current_r=int.from_bytes(tty.read(1), byteorder='little', signed=True),
            captor_l=int.from_bytes(tty.read(2), byteorder='little'),
            captor_r=int.from_bytes(tty.read(2), byteorder='little'),
            sync_stop=int.from_bytes(tty.read(1), byteorder='little')
        )


def log_until_moving(tty, csv):
    stops = 0
    while stops == 0:
        value = SerialRead.from_tty(tty)
        logging.info(value)
        csv.write('{}\n'.format(value.csv()))
        stops = max(value.steps_stop_l, value.steps_stop_r)


def log_until_stopped(tty, csv):
    stops = 1
    while stops > 0:
        value = SerialRead.from_tty(tty)
        logging.info(value)
        csv.write('{}\n'.format(value.csv()))
        stops = max(value.steps_stop_l, value.steps_stop_r)


def log_serial(tty, csv):
    log_until_moving(tty, csv)
    log_until_stopped(tty, csv)


def run_experiment(tty, csv, speed):
    tty.write(int.to_bytes(0, 3, 'little'))
    logging.info(hex(int.from_bytes(tty.read(1), byteorder='little')))

    sleep(1)

    tty.write(b'\x10')
    tty.write(int.to_bytes(10, 2, 'little', signed=True))

    tty.write(b'\x15')
    tty.write(int.to_bytes(speed, 2, 'little', signed=True))

    tty.write(b'\x16')
    tty.write(int.to_bytes(speed, 2, 'little', signed=True))

    log_serial(tty, csv)

    tty.write(b'\x10')
    tty.write(int.to_bytes(10, 2, 'little', signed=True))

    tty.write(b'\x15')
    tty.write(int.to_bytes(-speed, 2, 'little', signed=True))

    tty.write(b'\x16')
    tty.write(int.to_bytes(-speed, 2, 'little', signed=True))

    log_serial(tty, csv)

    tty.write(b'\x10')
    tty.write(int.to_bytes(20, 2, 'little', signed=True))

    tty.write(b'\x15')
    tty.write(int.to_bytes(speed, 2, 'little', signed=True))

    tty.write(b'\x16')
    tty.write(int.to_bytes(-speed, 2, 'little', signed=True))

    log_serial(tty, csv)

    tty.write(b'\x15')
    tty.write(int.to_bytes(-speed, 2, 'little', signed=True))

    tty.write(b'\x16')
    tty.write(int.to_bytes(speed, 2, 'little', signed=True))

    tty.write(b'\x10')
    tty.write(int.to_bytes(40, 2, 'little', signed=True))

    log_serial(tty, csv)

    tty.write(b'\x14')
    tty.write(int.to_bytes(0, 2, 'little', signed=True))


if __name__ == '__main__':
    try:
        with serial.Serial('/dev/ttyACM0', 9600) as tty:
            csv = open('data/sensehat_arduino_dump1.csv', 'w')
            csv.write('{}\n'.format(SerialRead.csv_header()))

            logging.info(tty.name)

            speeds = [25, 50, 75, 100]
            for speed in speeds:
                run_experiment(tty, csv, speed)

    except Exception as ex:
        logging.error(ex)
